#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdexcept>
#include <stack>

struct StackElement
{
    bool isElementWithX;
    int value;
};

struct Result 
{
    bool hasValue;
    int value;
};

Result getResult( std::string const & line );
bool isValidInteger( std::string const & line );
std::pair< StackElement, StackElement > getOperands( std::stack< StackElement > & operands );

int main()
{
    std::string line;

    while ( std::getline( std::cin, line ) ) 
    {
        Result result = getResult( line );

        std::cout << ( result.hasValue ? std::to_string( result.value ) : "Err" ) << " ";
    }

    std::cout << std::endl;

    return 0;
}

Result getResult( std::string const & line )
{
    std::stringstream lineStream( line );
    std::string element;

    lineStream >> element;

    if ( !isValidInteger( element ) )
    {
        throw std::invalid_argument( "Invalid y = " + element );
    }

    int y = std::stoi( element );

    std::stack< std::string > inputStack;

    while ( lineStream >> element ) 
    {
        inputStack.push( element );
    }

    int xMultiplier = 1;
    int xAdder = 0;
    int expressionMultiplier = 1;
    int expressionAdder = 0;
    std::stack< StackElement > operandStack;
    while ( !inputStack.empty() )
    {
        element = inputStack.top();
        inputStack.pop();
        
        if ( element == "*" )
        {
            auto operands = getOperands( operandStack );

            if ( !operands.first.isElementWithX && !operands.second.isElementWithX )
            {
                StackElement newElement;
                newElement.isElementWithX = false;
                newElement.value = operands.first.value * operands.second.value;
                operandStack.push( newElement );
            }
            else 
            {
                auto & operandValue = operands.first.isElementWithX ? operands.second.value : operands.first.value;

                if ( xAdder != 0 )
                {
                    expressionMultiplier *= operandValue;
                }
                else
                {
                    xMultiplier *= operandValue;
                }

                StackElement xElement;
                xElement.isElementWithX = true;
                operandStack.push( xElement );
            }
        }
        else if ( element == "+" )
        {
            auto operands = getOperands( operandStack );

            if ( !operands.first.isElementWithX && !operands.second.isElementWithX )
            {
                StackElement newElement;
                newElement.isElementWithX = false;
                newElement.value = operands.first.value + operands.second.value;
                operandStack.push( newElement );
            }
            else 
            {
                auto & operandValue = operands.first.isElementWithX ? operands.second.value : operands.first.value;

                if ( expressionMultiplier != 1 )
                {
                    expressionAdder += operandValue;
                }
                else
                {
                    xAdder += operandValue;
                }

                StackElement xElement;
                xElement.isElementWithX = true;
                operandStack.push( xElement );
            }
        }
        else if ( element == "-" )
        {
            auto operands = getOperands( operandStack );

            if ( !operands.first.isElementWithX && !operands.second.isElementWithX )
            {
                StackElement newElement;
                newElement.isElementWithX = false;
                newElement.value = operands.first.value - operands.second.value;
                operandStack.push( newElement );
            }
            else 
            {
                if ( operands.second.isElementWithX ) 
                {
                    auto & adder = expressionMultiplier == 1 ? xAdder : expressionAdder;
                    auto & multiplier = expressionMultiplier == 1 ? xMultiplier : expressionMultiplier; 

                    multiplier *= -1;
                    adder = operands.first.value - adder;
                }
                else if ( expressionMultiplier != 1 )
                {
                    expressionAdder -= operands.second.value;
                }
                else
                {
                    xAdder -= operands.second.value;
                }
                

                StackElement xElement;
                xElement.isElementWithX = true;
                operandStack.push( xElement );
            }
        }
        else if (element == "X" )
        {
            StackElement elementWithX;
            elementWithX.isElementWithX = true;
            operandStack.push( elementWithX );
        }
        else
        {
            if 
            ( !isValidInteger( element ) )
            {
                throw std::invalid_argument("Invalid element in the input equation, element: " + element );
            }

            StackElement stackInteger;
            stackInteger.isElementWithX = false;
            stackInteger.value = std::stoi( element );
            operandStack.push( stackInteger );
        }
    }

    Result result;

    y -= expressionAdder;

    if ( expressionMultiplier == 0 || y % expressionMultiplier != 0 )
    {
        result.hasValue = false;
        return result;
    }

    y /= expressionMultiplier;
    y -= xAdder;

    if ( xMultiplier == 0 || y % xMultiplier != 0 )
    {
        result.hasValue = false;
        return result;
    }

    y /= xMultiplier;

    result.hasValue = true;
    result.value = y;
    return result;
}

bool isValidInteger( std::string const & integer )
{
    auto beginIt = integer.begin();
    if ( *beginIt == '-' )
    {
        ++beginIt;
    }
    return std::all_of( beginIt, integer.end(), []( char e ) { return std::isdigit( e ); } );
}

std::pair< StackElement, StackElement > getOperands( std::stack< StackElement > & operands )
{
    std::pair< StackElement, StackElement > result;

    if ( operands.empty() )
    {
        throw std::invalid_argument( "Invalid equation!" );
    }

    result.first = operands.top();
    operands.pop();

    if ( operands.empty() )
    {
        throw std::invalid_argument( "Invalid equation! " );
    }

    result.second = operands.top();
    operands.pop();

    return result;
}