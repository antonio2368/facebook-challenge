CXX=g++
CFLAGS=-std=c++17

all: solution

solution: 	solution.o
			$(CXX) $^ -o $@

%.o: 	%.cpp
		$(CXX) -c $(CFLAGS) $^ -o $@